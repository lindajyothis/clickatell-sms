package com.cfgglobal.sms.web.job;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {

    @Autowired SmsWorker smsWorker;

    @Scheduled(fixedRate = 60_000)
    public void sendSms(){
        smsWorker.sendMessage();  //change parameters later
    }
}
