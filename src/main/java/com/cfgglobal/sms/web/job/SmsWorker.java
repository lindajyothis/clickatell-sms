package com.cfgglobal.sms.web.job;


import com.cfgglobal.sms.web.domain.SMSMessages;
import com.cfgglobal.sms.web.repository.SMSMessagesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;

@Component
public class SmsWorker {

    @Value("${clickatell.app.url}")
    private String clickatellAppUrl;

    @Value("${clickatell.username}")
    private String clickatellUsername;

    @Value("${clickatell.password}")
    private String clickatellPassword;

    @Value("${clickatell.api.id}")
    private String clickatellApiID;

    @Autowired
    private SMSMessagesRepository msgRepository;

    @Transactional
    public String sendMessage() {

        Integer status = 0;
        String result = "";
        List<SMSMessages> notSentMessages = msgRepository.findByStatus(status);

        if (notSentMessages.size() > 0) {
            //loop through the returned not-sent messages and send them one by one and update 0'status = 2' and 'sendon= sysdate' for them
            for (SMSMessages smsMessage : notSentMessages) {

                String urlParameters = "";
                String convertedMessage = "";
                String phoneNumber = smsMessage.getSendto();

                if (phoneNumber.matches("^[a-zA-Z]+$")) { // if its a text, we set the status to 3
                    smsMessage.setStatus(3);
                    smsMessage.setSendon(new Date());
                    msgRepository.save(smsMessage);

                } else {
                    String number = dealMobileNum(phoneNumber);
                    String content = smsMessage.getMessage();

                    try {
                        convertedMessage = byteToHex(content.getBytes("utf-16BE")).toLowerCase().replaceAll(" ", "");
                        urlParameters = String.format("user=%s&password=%s&api_id=%s&to=%s&text=%s&unicode=1",
                                URLEncoder.encode(clickatellUsername, "UTF-8"),
                                URLEncoder.encode(clickatellPassword, "UTF-8"),
                                URLEncoder.encode(clickatellApiID, "UTF-8"),
                                URLEncoder.encode(number, "UTF-8"),
                                URLEncoder.encode(convertedMessage, "UTF-8")
                        );
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    result = sendRequest(clickatellAppUrl, urlParameters);
                    // interpret the result
                    boolean errorFlag = false;
                    String resultMessages[] = result.split(": ");

                    if (resultMessages[0].toLowerCase().equals("err")) {
                        String errorMessages[] = resultMessages[1].trim().split(", ");
                        if (!(errorMessages[0].equals("301") || errorMessages[0].equals("901"))) {
                            errorFlag = true;
                        }

                    }
                    //  we are setting status = 2 except when we get result "301 - No credit left" or "901-Internal error - please retry". In these 2 cases, we want to retry.
                    // refer the codes here: https://archive.clickatell.com/developers/api-docs/http-error-codes/

                    if (errorFlag || result.startsWith("ID:")) {
                        smsMessage.setStatus(2);
                        smsMessage.setSendon(new Date());
                        msgRepository.save(smsMessage);
                    }
                }
            }
        }

        return result;
    }


    public String sendRequest(String targetURL, String urlParameters) {
        URL url;
        HttpURLConnection connection = null;
        StringBuffer response = new StringBuffer();
        try {

            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            // Send request
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            // Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;

            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\n');
            }
            rd.close();

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return response.toString();
    }

    private String dealMobileNum(String number) {

        if (number.startsWith("0")) {
            number = number.substring(1).trim();
        }
        if (number.startsWith("00")) {
            number = number.substring(2).trim();
        }
        if (number.startsWith("2")) {
            number = "64" + number;
        }
        if (number.startsWith("4")) {
            number = "61" + number;
        }
        if (number.startsWith("1")) {
            number = "86" + number;
        }

        return number;
    }

    public static String byteToHex(byte[] bt) {
        StringBuilder sb = new StringBuilder(4);
        for (int b : bt) {
            sb.append(Integer.toHexString(b & 0x00FF | 0xFF00).substring(2, 4)
                    .toUpperCase());
            sb.append(" ");
        }
        return sb.toString();
    }
}
