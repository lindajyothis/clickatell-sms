package com.cfgglobal.sms.web.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;


@Entity
@Data
public class SMSMessages implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private Integer seqnum;
    private String phonenumber;
    private String sendto;
    private String message;
    private Integer status;
    private Date createon;
    private Date sendon;
    private String customerid;

}
