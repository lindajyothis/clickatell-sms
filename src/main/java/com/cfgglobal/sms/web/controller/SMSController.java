package com.cfgglobal.sms.web.controller;


import com.cfgglobal.sms.web.service.SMSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class SMSController {

    @Autowired
    private SMSService smsService;


    @RequestMapping(value = {"/sms"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody  String smsSendFromMingyuan(@RequestParam(value = "num") String phone, @RequestParam(value = "content") String message) throws Exception {

        String responseMessage = smsService.sendMessage(phone, message);
        return responseMessage;

    }


    @RequestMapping(value = {"/goip/en/dosend.php"}, method = {RequestMethod.POST, RequestMethod.GET})
    public @ResponseBody  String smsSendFromOSS(@RequestParam(value = "smsnum") String phone, @RequestParam(value = "Memo") String message) throws Exception {
        phone = dealNZMobileNum(phone);
        String convertedMessage = byteToHex(message.getBytes("utf-16BE")).toLowerCase().replaceAll(" ", "");

        String responseMessage = smsService.sendMessage(phone, convertedMessage);
        return responseMessage;

    }

    private String dealNZMobileNum(String number) {
        if (number.startsWith("0")) {
            number = number.substring(1);
        }

        if (!number.startsWith("64")) {
            number = "64" + number;
        }

        return number;
    }



    public static String byteToHex(byte[] bt) {
        StringBuilder sb = new StringBuilder(4);
        for (int b : bt) {
            sb.append(Integer.toHexString(b & 0x00FF | 0xFF00).substring(2, 4)
                    .toUpperCase());
            sb.append(" ");
        }
        return sb.toString();
    }

}
