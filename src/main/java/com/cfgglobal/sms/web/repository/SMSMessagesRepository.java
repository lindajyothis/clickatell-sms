package com.cfgglobal.sms.web.repository;


import com.cfgglobal.sms.web.domain.SMSMessages;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SMSMessagesRepository extends CrudRepository<SMSMessages, Integer> {

    List<SMSMessages> findByStatus(Integer status);

}
