package com.cfgglobal.sms.web.service;

import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;

@Service
public class SMSServiceImpl implements SMSService {

    private static final String CLICKATELL_HTTP_BASE_URL = "https://api.clickatell.com/http/";
    private static final String CLICKATELL_USER_NAME = "myrapidpay";
    private static final String CLICKATELL_PASSWORD = "ZH0xywDR";
    private static final String CLICKATELL_API_ID = "3593635";


    public String sendMessage(String number, String messageContent) {

       String urlParameters = "";

        try {
            urlParameters = String.format("user=%s&password=%s&api_id=%s&to=%s&text=%s&unicode=1",
                    URLEncoder.encode(CLICKATELL_USER_NAME, "UTF-8"),
                    URLEncoder.encode(CLICKATELL_PASSWORD, "UTF-8"),
                    URLEncoder.encode(CLICKATELL_API_ID, "UTF-8"),
                    URLEncoder.encode(number, "UTF-8"),
                    URLEncoder.encode(messageContent, "UTF-8")
            );
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String result = sendRequest(CLICKATELL_HTTP_BASE_URL + "sendmsg", urlParameters);

        return result;
    }


    public String sendRequest(String targetURL, String urlParameters) {
        URL url;
        HttpURLConnection connection = null;
        StringBuffer response = new StringBuffer();
        try {

            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            // Send request
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            // Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;

            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\n');
            }
            rd.close();

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return response.toString();
    }
}
