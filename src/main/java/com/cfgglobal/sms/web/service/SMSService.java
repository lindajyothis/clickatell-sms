package com.cfgglobal.sms.web.service;


import java.net.UnknownHostException;

public interface SMSService {

    /**
     * @param number
     * @param messageContent
     * @return
     * @throws Exception
     */
    public String sendMessage(String number, String messageContent);

    /**
     * @param targetURL
     * @param urlParameters
     * @return
     * @throws UnknownHostException
     */

    public String sendRequest(String targetURL, String urlParameters);

}
