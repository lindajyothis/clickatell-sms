package com.cfgglobal.sms.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ImportResource("classpath:spring-config.xml")
public class SMSApplication extends SpringBootServletInitializer {


    public static void main(String[] args) {

        SpringApplication.run(SMSApplication.class, args);

    }
}